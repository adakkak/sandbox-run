#include "io_redirect.h"

/***** public functions *****/

/* redirect stream to file with name tag */
FILE *redirect_to_file(FILE **fp, char *name, int i, char *mode) {
	char buf[BUF_SIZE];
	sprintf(buf, "%s%d", name, i);
	return *fp = fopen(buf, mode);
}

/* check if fd is ready to read */
int is_fd_read_ready(int fd) {
	// set the fd_set and zero timeout
	fd_set set; FD_ZERO(&set); FD_SET(fd, &set);
	struct timeval timeout = (struct timeval){0};
	
	// call select and return if fd is set
	select(fd+1, &set, NULL, NULL, &timeout);
	return FD_ISSET(fd, &set);
}

/* pipe test case input to child */
int pipe_input_to_child(FILE *from_stream, int to_fd) {
	// read from_stream then write to_fd
	char c; int from_fd = fileno(from_stream);
	while (is_fd_read_ready(from_fd)) {
		if (read(from_fd, &c, 1) <= 0 || c <= 0) {
			c = EOF;
			break;
		}
		write(to_fd, &c, 1);
	}
	
	// delimit character is \x00, return 1 if end of test case
	if (c == EOF) {
		close(to_fd);
		return 1;
	}
	
	// return 0 if not end of test case
	return 0;
}

/* fork child and create pipes for redirection */
pid_t fork_with_pipes(int pipes[STDERR_FILENO+1][2], struct user_config *ucfg) {
	// create pipes for stdin, stderr, stdout will use terminal device
	int master_fd;
	pipe(pipes[STDIN_FILENO]);
	pipe(pipes[STDERR_FILENO]);
	
	// fork child using forkpty to prepare for line buffered stdout
	pid_t pid = forkpty(&master_fd, NULL, NULL, NULL);
	if (pid == -1) {
		// fork error
		return -1;
	} else if (pid == 0) {
		// child, redirect stdin, stderr to pipes
		dup2(pipes[STDIN_FILENO][R_END], STDIN_FILENO);
		close(pipes[STDIN_FILENO][W_END]);

		dup2(pipes[STDERR_FILENO][W_END], STDERR_FILENO);
		close(pipes[STDERR_FILENO][R_END]);
		return 0;
	}
	
	// parent, use the line buffered terminal fd
	pipes[STDOUT_FILENO][R_END] = master_fd;
	pipes[STDOUT_FILENO][W_END] = -1;
	
	// close unused pipe ends
	close(pipes[STDIN_FILENO][R_END]);
	close(pipes[STDERR_FILENO][W_END]);
	
	// set terminal attribute
	struct termios term;
	tcgetattr(master_fd, &term);
	term.c_oflag &= ~ONLCR;		// disable \r\n line breaking
	tcsetattr(master_fd, TCSANOW, &term);
	return pid;
}
