#include "result_json.h"

/* constants */
#define RES_FILE	"result.json"

/***** public functions *****/

/* print result in json */
void print_result_json(struct user_config *ucfg) {
	// do not print if silence mode is on
	if (ucfg->silence_mode) return;
	
	// print to file or stdout
	FILE *to_stream = ucfg->redir_result ? fopen(RES_FILE, "w") : stdout;
	fprintf(to_stream, "{\n\"result\":[%s],\n", ucfg->res.result);
	fprintf(to_stream, "\"execute_time\":%lf,\n", ucfg->res.execute_time);
	fprintf(to_stream, "\"memory_usage\":%d\n}\n", ucfg->res.memory_usage);
	if (ucfg->redir_result) fclose(to_stream);
}

/* convert return code to message string */
const char *strcode(int code) {
	// known return code
	switch (code) {
		case OK: return "OK";
		case CE: return "Compile Error";
		case ENF: return "Executable Not Found";
		case RE: return "Runtime Error";
		case TLE: return "Time Limit Exceed";
		case MLE: return "Memory Limit Exceed";
		case OLE: return "Output Limit Exceed";
		case SRE: return "System Resource Exceed";
		case BANNED: return "Bad System Call";
	}
	
	// other unknow error
	return "Judge Error";
}
