#ifndef COMPILE_CODE
#define COMPILE_CODE

#include "common.h"

/* prototypes */
int compile_code(struct user_config *ucfg);

#endif /* COMPILE_CODE */
