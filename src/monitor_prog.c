#include "monitor_prog.h"

/* private functions */

/* setup syscall filter */
static void install_syscall_filter() {
	// ensure no new privilages can be granted
	prctl(PR_SET_NO_NEW_PRIVS, 1);
	prctl(PR_SET_DUMPABLE, 0);
  
	// setup blacklist
	scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
	
	// process related
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(fork), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(vfork), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(clone), 1,
		SCMP_A1(SCMP_CMP_EQ, 0));	// child_stack = 0 is a fork() call
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(kill), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(sigaction), 0);
	
	// file related
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(open), 1,
		SCMP_A1(SCMP_CMP_MASKED_EQ, O_ACCMODE, O_WRONLY));	// write mode
	/* blocking O_RDWR will cause nodejs not work, need other approach */
	//seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(open), 1,
	//	SCMP_A1(SCMP_CMP_MASKED_EQ, O_ACCMODE, O_RDWR));	// read write mode
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(open), 1,
		SCMP_A1(SCMP_CMP_MASKED_EQ, O_CREAT, O_CREAT));		// create mode
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(open), 1,
		SCMP_A1(SCMP_CMP_MASKED_EQ, O_TRUNC, O_TRUNC));		// truncate mode
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(open), 1,
		SCMP_A1(SCMP_CMP_MASKED_EQ, O_APPEND, O_APPEND));	// append append
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(unlink), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(unlinkat), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(rename), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(chdir), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(mkdir), 0);
	
	// socket related
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(bind), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(setsockopt), 0);
	seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(listen), 0);
	
	// apply the filter
	seccomp_load(ctx);
}

/***** public functions *****/

/* monitor the forked child */
int monitor_program(struct user_config *ucfg, FILE *log_file, double *elapsed, int *mem_peak) {
	// array for pipe fds
	int pipes[STDERR_FILENO+1][2];
	
	// fork a child
	pid_t pid = fork_with_pipes(pipes, ucfg);
	if (pid < 0) {
		// error
		lperror(log_file, "forkpty");
		return ERR;
	} else if (pid == 0) {
		// handle child, set a non root uid
		if (getuid() == 0 && setuid(1000) < 0) {
			perror("setuid");
			_exit(ERR);
		}
		
		// start ptrace
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		
		// install system call filter before exec
		install_syscall_filter();
		execv(ucfg->exec_file[0], ucfg->exec_file);
		
		// failed, use _exit() to exit immediately, otherwise strange behavior
		perror("execv");
		_exit(ERR);
	}
	
	// parent start here, initialize end of test case indicator
	int eot = 0;
	
	// initialize limit checkers
	int limit_exceed = 0, total_bytes = 0;
	check_mle(NULL, pid);
	check_tle(NULL, 0);
	
	while (1) {
		// wait in non blocking mode in order to handle pipe
		int status, ret = waitpid(pid, &status, WNOHANG);
		if (ret == -1) {
			lperror(log_file, "wait4");
			break;
		}
		
		// pipe stdin to child in non blocking mode
		if (!eot) {
			eot = pipe_input_to_child(ucfg->in, pipes[STDIN_FILENO][W_END]);
		}
		
		if (!limit_exceed) {
			// check OLE
			if (check_ole(pipes[STDOUT_FILENO][R_END], ucfg->out, &total_bytes, ucfg->out_lim) ||
				check_ole(pipes[STDERR_FILENO][R_END], ucfg->err, &total_bytes, ucfg->out_lim)) {
				lprintf(log_file, "OLE detected\n");
				kill(pid, SIGXFSZ);
				limit_exceed = OLE;
			}
			
			// check TLE
			if (check_tle(elapsed, ucfg->time_lim)) {
				lprintf(log_file, "TLE detected\n");
				kill(pid, SIGXCPU);
				limit_exceed = TLE;
			}
			
			// check MLE
			if (check_mle(mem_peak, ucfg->mem_lim)) {
				lprintf(log_file, "MLE detected\n");
				kill(pid, SIGSEGV);
				limit_exceed = MLE;
			}
		}
		
		// child changed state
		if (ret == pid) {
			// log usage
			lprintf(log_file, "\t>>>>> Child changed state <<<<<\n");
			lprintf(log_file, "Time elapsed: %lf s\n", *elapsed);
			lprintf(log_file, "Output size: %d bytes\n", total_bytes);
			lprintf(log_file, "VmHWM: %d bytes\n", *mem_peak);
			
			// child exits peacefully
			if (WIFEXITED(status)) {
				lprintf(log_file, "WIFEXITED\n");
				ret = WEXITSTATUS(status);
				lprintf(log_file, "User program returned %d\n", ret);
				
				// return status code
				ret = ret == 0 ? OK : RE;
			// child is signaled or stopped
			} else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
				if (WIFSIGNALED(status)) {
					// child is signaled
					lprintf(log_file, "WIFSIGNALED\n");
					ret = WTERMSIG(status);
					lprintf(log_file, "Signal which kill program: %d %s\n", ret, strsignal(ret));
				} else if (WIFSTOPPED(status)) {
					// child is stopped
					lprintf(log_file, "WIFSTOPPED\n");
					ret = WSTOPSIG(status);
					lprintf(log_file, "Signal which stop program: %d %s\n", ret, strsignal(ret));
				}
				
				// return corresponding status code
				if (limit_exceed) {
					ret = limit_exceed;
				} else if (ret == SIGTRAP) {
					// ptrace stop for checking
					ptrace(PTRACE_CONT, pid, 0, 0);
					continue;
				} else {
					// child must die signal
					kill(pid, SIGKILL);
					switch (ret) {
						case SIGSYS: ret = BANNED; break;
						case SIGXCPU: ret = TLE; break;
						case SIGXFSZ: ret = OLE; break;
						case SIGSEGV: ret = RE; break;
						case SIGFPE: ret = RE; break;
						default: ret = ERR;
					}
				}
			}
			
			// except SIGTRAP, child must be dead already, close all pipes
			close(pipes[STDIN_FILENO][W_END]);
			close(pipes[STDOUT_FILENO][R_END]);
			close(pipes[STDERR_FILENO][R_END]);
			return ret;
		}
		
		// sleep for 1 ms to prevent busy looping
		usleep(1000);
	}
	
	// must be error if this line can be reached
	return ERR;
}
