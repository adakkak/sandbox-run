#ifndef PARSE_OPTS
#define PARSE_OPTS

#include "common.h"
#include <ctype.h>

/* prototypes */
void display_usage();
int parse_argv(struct user_config *ucfg, int argc, char **argv);

#endif /* PARSE_OPTS */
