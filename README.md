# **What is Sandbox-Run**

*This is part of a FYP project and is for educational use.*

Sandbox-Run aims to sandbox the **compilation** and **excution** of **source code** or **scripts**. Being run inside Docker, this makes Sandbox-Run **cross-platform**.

Sandbox-Run can be used in an Online Judge System, facilitates high protection against dangerous code. Or it can just be used to compile and run untrusted code or scripts.

Link to [**Docker Hub Repo**](https://hub.docker.com/r/tomlau10/sandbox-run/)

Link to [**Bitbucket Wiki Page**](https://bitbucket.org/tomlau10/sandbox-run/wiki/Home)

Link to [**Bitbucket Issue Tracker**](https://bitbucket.org/tomlau10/sandbox-run/issues)



# **Usage**

```
$ docker run [docker-run-options] tomlau10/sandbox-run [options] <executable> [args]
```

For detailed reference, please visit [**Bitbucket Wiki Page**](https://bitbucket.org/tomlau10/sandbox-run/wiki/Home)



# **Technology**
Sandbox-Run is written in C language for efficiency concern

* Uses **ptrace** to monitor the `<executable>`
* Uses **libseccomp** to block potentially dangerous system call

Built on the [**alpine:edge**](https://registry.hub.docker.com/_/alpine/) docker image which has minimal size.

![Alpine Linux](https://raw.githubusercontent.com/docker-library/docs/master/alpine/logo.png)

Then pre-installed a few compilers and interpreters:

* `gcc`
* `nodejs`
* `python`
* `ruby`



# **Development**

To compile and build:

The binary should be built inside the Alpine Linux container. `build.sh` is prepared for compiling the binary, building the image locally and removing untagged docker images in one step.

```
$ ./build.sh
```

For more details of the building process, please refer to the `Dockerfile`.
