/* include library */
var socketioclient = require('socket.io-client');
var fs = require('fs');

/* read config.json */
var config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

var largeStdin63KB = ''
for (var i = 63 * 1024; i >= 0; i--) {
	largeStdin63KB += '1'
};
var largeStdin64KB = ''
for (var i = 65 * 1024; i >= 0; i--) {
	largeStdin64KB += '1'
};
/* test cases defined below */
var testcase = [
	// c
	{
		'user' : 'C OK',
		'code' : '#include <stdio.h>\n int main() {fprintf(stderr,"no error!\\n");printf("Hello World!"); return 0;}',
		'language' : 'C'
	},
	{
		'user' : 'C OLE',
		'code' : '#include <stdio.h>\n int main() {while(1)printf("Hello World!"); return 0;}',
		'language' : 'C'
	},
	{
		'user' : 'C TLE',
		'code' : '#include <stdio.h>\n int main() {while(1); return 0;}',
		'language' : 'C'
	},
	{
		'user' : 'C MLE',
		'code' : '#include <string.h>\n#include <stdlib.h>\n int main() {\nwhile(1) {char* c = malloc(1024*1024);memset(c,-1,1024*1024);\n} return 0;}',
		'language' : 'C'
	},
	{
		'user' : 'C CE',
		'code' : '#include <stdio.h>\n int main() {printf("Hello World!") return 0;}',
		'language' : 'C'
	},
	{
		'user' : 'C Test Cases',
		'code' : '#include <stdio.h>\n int main() {int d; scanf("%d", &d); printf("%d\\n", d); return 0;}',
		'language' : 'C',
		'setting' : {
			'number_of_testcase': 5
		},
		'stdin' : ['2', '3', '4', '10000', '0.2']
	},
	{
		'user' : 'C OK With OLE In Middle',
		'code' : '#include <stdio.h>\n int main() {int d; scanf("%d", &d); if (d==4) while (1) puts(""); printf("%d\\n", d); return 0;}',
		'language' : 'C',
		'setting' : {
			'number_of_testcase': 5
		},
		'stdin' : ['2', '3', '4', '10000', '0.2']
	},
	{
		'user' : 'C Fail In Middle',
		'code' : '#include <stdio.h>\n int main() {int d; scanf("%d", &d); if (d==4) while (1); printf("%d\\n", d); return 0;}',
		'language' : 'C',
		'setting' : {
			'number_of_testcase': 5
		},
		'stdin' : ['2', '3', '4', '10000', '0.2']
	},
	{
		'user' : 'C Fork Bomb',
		'code' : '#include <unistd.h>\n int main(void) {while(1) fork();}',
		'language' : 'C'
	},
	
	// python
	{
		'user' : 'Python OK',
		'code' : 'print "Hello World!"',
		'language' : 'Python'
	},
	{
		'user' : 'Python Fork Bomb',
		'code' : 'import os\nwhile (1):\n\tos.fork()',
		'language' : 'Python'
	},
	
	// javascript
	{
		'user' : 'JavaScript OK',
		'code' : 'console.log("Hello World!")',
		'language' : 'JavaScript'
	},
	{
		'user' : 'JavaScript Fork Bomb',
		'code' : 'var exec = require("child_process").exec; while(1) exec(__filename)',
		'language' : 'JavaScript'
	},
	
	// ruby
	{
		'user' : 'Ruby OK',
		'code' : 'puts "Hello World!"',
		'language' : 'Ruby'
	},
	{
		'user' : 'Ruby Fork Bomb',
		'code' : 'loop { fork }',
		'language' : 'Ruby'
	},
	{
		'user' : 'Large input OK',
		'code' : '#include <stdio.h>\n int main() {printf("Hello World!"); return 0;}',
		'language' : 'C',
		'setting': {
			'number_of_testcase': 1
		},
		'stdin' : [largeStdin63KB]
	},
	{
		'user' : 'Large input Fail',
		'code' : '#include <stdio.h>\n int main() {printf("Hello World!"); return 0;}',
		'language' : 'C',
		'setting': {
			'number_of_testcase': 1
		},
		'stdin' : [largeStdin64KB]
	},
	{
		'user' : 'print null',
		'code' : '#include <stdio.h>\n int main() {printf("b4 null %c should be in the same output as b4 null\\n", 0); return 0;}',
		'language' : 'C',
		'setting': {
			'number_of_testcase': 2
		},
		'stdin' : ['foo', 'bar']
	}
];
var submissionID, pressureTestCountdown;
var pressureTestRound = 5;

/* connect to docker judge server */
var socket = socketioclient.connect('http://localhost:' + config.port);

/* on connect */
console.log('waiting for connection');
socket.on('connect', function () {
	console.log('connected');
	submissionID = 0;
	pressureTestCountdown = testcase.length;
	fireSubmission();
});
socket.on('disconnect', function () {
	console.log('disconnected');
	pressureTestCountdown = -1;
});

/* on result */
socket.on('result', function (result) {
	console.log(result);
	console.log('');
	
	// invoke pressure test when count down reaches zero
	pressureTestCountdown--;
	if (!pressureTestCountdown) {
		console.log('press <enter> to invoke pressure test or <ctrl+c> to quit test');
	}
});

/* fire a round of submission */
function fireSubmission() {
	for (var t in testcase) {
		// log the ID and user
		var submission = testcase[t], user = submission.user;
		
		// add an ID to user field
		submission.user = submissionID + ' ' + user;
		console.log(submission.user);
		socket.emit('sandbox', submission);

		// restore user field
		submission.user = user;
		submissionID++;
	}
}

/* invoke pressure test */
process.stdin.on('data', function pressureTest() {
	// count down reached, invoke pressure test
	if (!pressureTestCountdown) {
		submissionID = 0;
		pressureTestCountdown = pressureTestRound * testcase.length;
		for (var i=0; i<pressureTestRound; i++) fireSubmission();
	}
});
