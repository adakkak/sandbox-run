/* include library */
var fs = require('fs');
var moment = require('moment');
var express = require('express');
var cp = require('child_process');
var http = require("http");

/* read config.json */
var config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

/* create http server */
var expressServer = express();
var httpServer = expressServer.listen(config.port);

/* create socket server */
var socketio = require('socket.io')
var socketServer = socketio.listen(httpServer);

/* submission queue and control */
var submissionQueue = [], jobCount = 0;
var EventEmitter = require('events').EventEmitter;
var submissionControl = new EventEmitter();
submissionControl.on('tryToProcess', function (jobFinished) {
	// decrement job count if finished a job
	if (jobFinished) {
		if (jobCount > 0) jobCount--;
		console.log('Sandbox: Job finish, doing/queueing = ' + jobCount + '/' + submissionQueue.length);
	}
	
	// try to process another
	if (jobCount >= config.job_pool_size) {
		console.log('Sandbox: Job queued, doing/queueing = ' + jobCount + '/' + submissionQueue.length);
	} else if (submissionQueue.length > 0) {
		var job = submissionQueue.shift();
		if (job.socket.disconnected) {
			console.log('Sandbox: Ignore job, doing/queueing = ' + jobCount + '/' + submissionQueue.length);
			submissionControl.emit('tryToProcess');
		} else {
			jobCount++;
			console.log('Sandbox: Adding job, doing/queueing = ' + jobCount + '/' + submissionQueue.length);
			processSubmission(job.socket, job.submission);
		}
	}
});

/* submission record and status page */
var record = [];
expressServer.get('/status', function (req, res) {
	// send status in most-recent order
	var status = [
		"jobCount = " + jobCount,
		"queue.length = " + submissionQueue.length,
		"most recent submission:"
	];
	for (var i = record.length-1; i >= 0; i--) {
		var r = record[i];
		var status_str = r.submitTime + ' ' + r.user;
		status.push(status_str);
	}
  	res.send(status.join('</br>'));
});

/* helper functions */
function getLangaugeExt(language) {
	// convert language string to ext
	switch (language.toLowerCase()) {
		case 'c': return '.c';
		case 'python': return '.py';
		case 'ruby': return '.rb';
		case 'javascript': return '.js';
	}

	// unknown
	return '';
}

function getLanguageStr(language) {
	// convert language short from to full name
	language = language.toLowerCase()
	switch (language) {
		case 'c':
		case 'python':
		case 'ruby':
		case 'javascript':
			return language;
	}
	
	// unknown
	return '';
}

function posixPath(dir) {
	// convert window format path to posix format
	if (dir.indexOf(':') > -1) {
		// fix cmd and cygwin capitalize error
		dir = dir[0].toLowerCase() + dir.slice(1);
		// add root slash, remove colon and replace all blackslash
		dir = '/' + dir.replace(':', '').replace(/\\/g, '/');
	}
	return dir;
}

/* on new submission */
socketServer.on('connection', function (socket) {
	console.log('Sandbox: ' + socket.handshake.address + ' connected at ' + socket.handshake.time);
	socket.on('sandbox', function (submission) {
		// push to queue and try to process
		submission.submitTime = moment().format(config.date_format);
		submissionQueue.push({ socket:socket, submission:submission });
		submissionControl.emit('tryToProcess');
	});
});

/* on process submission */
function processSubmission(socket, submission) {
	// store the recent submission and present it on the page
	while (record.length >= 100) record.shift();
	record.push(submission);
	//console.log(submission);

	// create sandbox folder
	submission.startTime = moment().format(config.date_format);
	var containerName = submission.startTime + '-' + submission.user.replace(/_/g, '__').replace(/ /g, '_');
	var sandboxPath = __dirname + '/submission/' + containerName;
	fs.mkdirSync(sandboxPath);

	// get language code and string
	var langStr = getLanguageStr(submission.language);
	var langExt = getLangaugeExt(langStr);

	// read setting
	var setting = {}, sub_set = submission.setting, def_set = config.default_setting[langStr];
	if (sub_set) {
		setting.number_of_testcase = sub_set.number_of_testcase || 0;
		setting.time_limit = sub_set.time_limit || def_set.time_limit;
		setting.memory_limit = sub_set.memory_limit || def_set.memory_limit;
		setting.output_limit = sub_set.output_limit || def_set.output_limit;
	} else {
		setting = def_set;
		setting.number_of_testcase = 0;
		submission.setting = setting;
	}
	//console.log(setting);

	// put the code into the sandbox folder
	var codeFile = 'code' + langExt;
	fs.writeFileSync(sandboxPath + '/' + codeFile, submission.code);

	// change window path to linux format if necessary and build the cmd string
	var security_option = process.platform === 'linux' ? '--security-opt apparmor:unconfined' : '';
	var sandbox_mount = posixPath(sandboxPath) + ':/vol/ ';
	var setuid_option = process.platform === 'linux' ? '-u $(id -u):$(id -g)' : '';
	var cmd = [
		'docker', 'run',					// docker run
		config.use_pipe ? '-i' : '',		// interactive mode if enable piping
		'--name', containerName,			// container name
		'--net', 'none',					// disable network in container
		security_option,					// fix ptrace with apparmor problem in ubuntu
		'-v', sandbox_mount,				// sandbox mount
		setuid_option,						// set uid properly on ubuntu
		'sandbox-run',						// the sandbox-run image
		config.use_pipe ? '' : '-IOECR',	// redirect flag if not enable piping
		config.enable_logging ? '-l' : '',	// -l switch if enable logging
		'-e', config.error_tolerance_level,	// error tolerance level
		'-n', setting.number_of_testcase,	// number of test case input
		'-t', setting.time_limit,			// time limit
		'-m', setting.memory_limit,			// memory limit
		'-o', setting.output_limit,			// output limit
		'-c', codeFile, 'executable'		// source file and executable
	].join(' ');
	//console.log(cmd);
	
	if (!config.use_pipe) {
		// write stdin files to sandbox path
		for (var i=0; i<setting.number_of_testcase; i++)
			fs.writeFileSync(sandboxPath + '/stdin' + i, submission.stdin[i]);
	}
	
	// create a docker container to run
	var options = { socket:socket, submission:submission, sandboxPath:sandboxPath, containerName:containerName };
	var child = cp.exec(cmd, { timeout:20000 }, onSandboxDone.bind(null, options));
	
	if (config.use_pipe) {
		// pipe stdin to child
		var data = submission.stdin ? submission.stdin.join('\x00') : '';
		child.stdin.write(data, function () { child.stdin.end(); });
	}
}

/* on sandbox done */
function onSandboxDone(options, error, stdout, stderr) {
	var socket = options.socket;
	var submission = options.submission;
	var sandboxPath = options.sandboxPath;
	var containerName = options.containerName;
	
	// remove the container anyway
	cp.exec("docker rm -f " + containerName);
	
	// read result json
	var resultObj = {}, resultJSON, outArr = [], errArr = [];
	try {
		if (config.use_pipe) {	// read data from pipes
			// split stdout and stderr
			outArr = stdout.split('\x00');
			errArr = stderr.split('\x00');
			
			// outArr[last] is result.json while errArr[last] is unused
			resultJSON = outArr.pop(); errArr.pop();
		} else {	// read data from files
			// read from result.json
			resultJSON = fs.readFileSync(sandboxPath + '/result.json', 'utf8');
		}
		resultObj = JSON.parse(resultJSON);
	} catch (e) {
		// server side error
		if (error) console.log(error);
		if (stdout) console.log('stdout: ' + stdout);
		if (stderr) console.log('stderr: ' + stderr);
		console.log(resultJSON);
		resultObj = { result: ['Server Side Error'] };
	}

	// read compile message
	if (config.use_pipe) {
		resultObj.compile_message = outArr.shift() + errArr.shift() || '';
	} else try {
		resultObj.compile_message = fs.readFileSync(sandboxPath + '/cmsg.txt', 'utf8');
	} catch (e) {
		resultObj.compile_message = '';
	}

	// read stdout and stderr
	for (var i = 0; i < resultObj.result.length; i++) {
		if (resultObj.result[i] === 'Server Side Error') {
			// append empty string if server side error
			outArr.push('');
			errArr.push('');
		} else if (!config.use_pipe) {
			// read from files if use_pipe
			outArr.push(fs.readFileSync(sandboxPath + '/stdout' + i, 'utf8'));
			errArr.push(fs.readFileSync(sandboxPath + '/stderr' + i, 'utf8'));
		}
		
		// ignore output for OLE cases
		if (resultObj.result[i] === 'Output Limit Exceed') {
			if (outArr[i]) outArr[i] = '(...)';
			if (errArr[i]) errArr[i] = '(...)';
		}
	}
	resultObj.stdout = outArr;
	resultObj.stderr = errArr;
	
	// set total time used and user
	var finishTime = moment();
	resultObj.total_time = finishTime.diff(moment(submission.submitTime, config.date_format)) / 1000;
	submission.finishTime = finishTime.format(config.date_format);
	resultObj.user = submission.user;
	
	// async save stdin/out/err, result and submission json for future reference
	if (config.use_pipe && config.enable_logging) {
		if (submissions.stdin) {
			for (var i = 0; i < submission.stdin.length; i++) {
				fs.writeFile(sandboxPath + '/stdin' + i, submission.stdin[i], 'utf8');
			}
		}
		if (resultObj.stdout) {
			for (var i = 0; i < resultObj.stdout.length; i++) {
				fs.writeFile(sandboxPath + '/stdout' + i, resultObj.stdout[i], 'utf8');
				fs.writeFile(sandboxPath + '/stderr' + i, resultObj.stderr[i], 'utf8');
			}
		}
	}
	fs.writeFile(sandboxPath + '/result.json', JSON.stringify(resultObj, null, '\t'));
	fs.writeFile(sandboxPath + '/submission.json', JSON.stringify(submission, null, '\t'));

	// send result and emit 'tryToProcess' as job finished
	console.log(resultObj); console.log('');
	submissionControl.emit('tryToProcess', true);
	socket.emit(submission.reply || 'result', resultObj);
}

/* initialize */
(function init() {
	console.log('Sandbox: initializing...');
	
	// handle signal for clean up
	process.on('SIGINT', cleanUp)
		.on('SIGTERM', cleanUp)
		.on('SIGQUIT', cleanUp)
		.on('SIGSEVG', cleanUp)
		.on('SIGHUP', cleanUp);
	
	function cleanUp(options) {
		// remove all containers
		console.log('Sandbox: removing any docker containers...');
		try {
			var ids = cp.execSync('docker ps -aq', { encoding:'utf8' }).replace(/\x0a/g, ' ');
			if (ids) console.log(cp.execSync('docker rm -f ' + ids, { encoding:'utf8' }));
		} catch (e) {};
		
		// exit if not an initialization
		if (!options || !options.init) process.exit();
	}
	
	// clean up at start also
	cleanUp({ init:true });
	
	// create submission folder if not exist
	try {
		if (!fs.lstatSync(__dirname + '/submission').isDirectory()) {
			fs.unlinkSync(__dirname + '/submission');
			throw new Error('not a directory');
		}
	} catch (e) {
		fs.mkdirSync(__dirname + '/submission');
    }
	
	// ready
	console.log('Sandbox: initialized and ready for connection.');
})();
