# Single Use Example
This example shows that Sandbox-Run can be used to run untrusted source code.
The `a_ne_b.c` is a a+b program in C language, which will call fork when `a==b`.
Sandbox-Run will be able to capture dangerous system call when it is triggered.

# Interactive Test
1. Run `node run.js`
2. Treat it as a simple a+b program
3. When `a==b`, the program should be terminated
