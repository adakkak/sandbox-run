/* require library */
var cp = require('child_process');

/* convert window format path to posix format */
function posixPath(dir) {
	if (dir.indexOf(':') > -1) {
		dir = dir[0].toLowerCase() + dir.slice(1);
		dir = '/' + dir.replace(':', '').replace(/\\/g, '/');
	}
	return dir;
}

/* build docker command */
var contName = 'interactive_test';
var security_option = process.platform === 'linux' ? '--security-opt apparmor:unconfined' : '';
var sandbox_mount = posixPath(__dirname) + ':/vol';
var setuid_option = process.platform === 'linux' ? '-u $(id -u):$(id -g)' : '';
var cmd = [
	"docker", "run", "-i",	// docker run with interactive mode
	'--name', contName,		// container name
	security_option,		// fix ptrace with apparmor problem in ubuntu
	'-v', sandbox_mount,	// sandbox mount
	setuid_option,			// set uid properly on ubuntu
	'sandbox-run',			// the sandbox run image
	"-c", 'a_ne_b.c',		// source code file
	"./binary"				// executable path
].join(' ');

/* exec the cmd */
console.log(cmd);
var child = cp.exec(cmd);

/* pipe process stdin to child */
process.stdin.on('data', function (data) {
	child.stdin.write(data);
});

/* setup child listener */
(function childListener() {
	// after cmsg will be a \x00, after output will also be a \x00
	var expectX00 = 2;
	
	// close child stdin also on process stdin close
	process.stdin.on('end', function () {
		child.stdin.end();
	});
	
	// on stdout data
	child.stdout.on('data', function (data) {
		// if no more expectX00, close stdin to finish
		expectX00 -= data.split('\x00').length - 1;
		if (!expectX00) child.stdin.end();
		
		process.stdout.write("stdout: " + data);
	});

	// on stderr data
	child.stderr.on('data', function (data) {
		process.stderr.write("stderr: " + data);
	});

	// child exit
	child.on('exit', function (code) {
		console.log('child exited with ' + code);
		
		// kill container then exit
		cp.exec("docker rm -f " + contName, function () {
			process.exit();
		});
	});
})();
